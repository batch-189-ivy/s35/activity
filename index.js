
const express = require('express')

const mongoose = require('mongoose')

const app = express()

const port = 3001

mongoose.connect("mongodb+srv://ivybacudo:admin123@cluster0.merm9.mongodb.net/S35-Activity?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection Error"))

db.on('open', () => console.log('Connected to MongoDB!'))

//Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

//Model
const User = mongoose.model('User', userSchema)


//Route
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//Single route
app.post('/signup', (req, res) =>{
	User.findOne({username: req.body.username}, (error, result) => {
		if(result !== null && result.username == req.body.username){
			return res.send('Duplicate user found!')
		} else {
			if(req.body.username !== '' && req.body.password !== ''){
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				})
				newUser.save((error, savedUser) =>{
					if(error){
						return res.send(error)
					}
					return res.send('New User registered successfully!')
				})
			} else {
				return res.send(`BOTH Username and Password must be provided.`)
			}
		}
	})
})



app.listen(port, () => console.log(`Server is running at port ${port}`))
